package vn.tiki.android.androidhometest.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule
import vn.tiki.android.androidhometest.R
import vn.tiki.android.androidhometest.data.api.response.Deal
import vn.tiki.android.androidhometest.util.CountDownUtils
import java.util.*

@GlideModule
class MyAppGlideModule : AppGlideModule()

class DealViewHolder(view: View, private val context: Context,
                     private val removeItemCallback: (deal: Deal) -> Unit) : RecyclerView.ViewHolder(view) {

    private var imgAvatar: ImageView = itemView.findViewById(R.id.img_avatar) as ImageView
    private var tvNameName: TextView = itemView.findViewById(R.id.txt_name)
    private var tvPrice: TextView = itemView.findViewById(R.id.txt_price)
    private var tvCountDown: TextView = itemView.findViewById(R.id.txt_count_down)

    private lateinit var deal: Deal

    private var countDownUtils: CountDownUtils? = null

    fun setDeal(deal: Deal) {
        this.deal = deal
        tvNameName.text = deal.productName
        tvPrice.text = context.resources.getString(R.string.price_with_value, deal.productPrice)
        GlideApp.with(context)
                .load(deal.productThumbnail)
                .centerCrop()
                .into(imgAvatar)

        //timer
        val timerLengthSeconds = getTimerLengthSeconds(deal.endDate)
        //reset count down
        if (countDownUtils != null) {
            countDownUtils!!.cancelTimer()
            countDownUtils = null
            tvCountDown.text = ""
        }
        countDownUtils = CountDownUtils(context, timerLengthSeconds) { remainingTime ->
            countdownCallback(remainingTime)
        }
        countDownUtils!!.initTimer()

    }

    private fun getTimerLengthSeconds(date: Date): Long {
        return (date.time - Date().time) / 1000
    }

    private fun countdownCallback(remainingTime: String) {
        if (remainingTime != "0") {
            tvCountDown.text = remainingTime
        } else {
            removeItemCallback(deal)
        }
    }
}