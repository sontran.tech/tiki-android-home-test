package vn.tiki.android.androidhometest.util

import android.content.Context
import android.os.CountDownTimer

class CountDownUtils(val context: Context, private var secondsRemaining: Long = 0,
                     private val countdownCallback: (callbackValue: String) -> Unit) {

    private lateinit var timer: CountDownTimer

    fun initTimer() {
        startTimer()
        updateCountdownUI()
    }

    private fun onTimerFinished() {
        countdownCallback("0")
        cancelTimer()
    }

    fun cancelTimer() {
        //in case timer changed itself to null
        if (timer != null) {
            timer.cancel()
        }
    }

    private fun startTimer() {
        timer = object : CountDownTimer(secondsRemaining * 1000, 1000) {
            override fun onFinish() = onTimerFinished()

            override fun onTick(millisUntilFinished: Long) {
                //update UI every second
                secondsRemaining = millisUntilFinished / 1000
                updateCountdownUI()
            }
        }.start()
    }

    private fun updateCountdownUI() {
        val hoursUntilFinished = secondsRemaining / 3600
        val hoursStr = formatStringToTime(hoursUntilFinished.toString())

        val minutesUntilFinished = (secondsRemaining / 60) - hoursUntilFinished * 60
        val minutesStr = formatStringToTime(minutesUntilFinished.toString())

        val secondsInMinuteUntilFinished = secondsRemaining - minutesUntilFinished * 60
        val secondsStr = formatStringToTime(secondsInMinuteUntilFinished.toString())

        val callbackValue = "$hoursStr:$minutesStr:$secondsStr"

        countdownCallback(callbackValue)
    }

    private fun formatStringToTime(inputStr: String): String {
        if (inputStr.length == 1) {
            return "0$inputStr"
        }
        return inputStr
    }
}