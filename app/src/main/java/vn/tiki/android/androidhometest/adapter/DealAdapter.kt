package vn.tiki.android.androidhometest.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import vn.tiki.android.androidhometest.R
import vn.tiki.android.androidhometest.data.api.response.Deal


class DealAdapter(private var mDeals: MutableList<Deal>) : RecyclerView.Adapter<DealViewHolder>() {
    private var onBind: Boolean = false

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): DealViewHolder {
        val context = viewGroup.context
        val layoutInflater = LayoutInflater.from(context)
        val v = layoutInflater.inflate(R.layout.item_deal, null)

        return DealViewHolder(v, context) { deal -> removeItem(itemNeedsToBeRemoved = deal) }
    }

    override fun onBindViewHolder(holder: DealViewHolder, position: Int) {
        onBind = true
        val deal = mDeals[position]
        holder.setDeal(deal)
        onBind = false
    }

    override fun getItemCount(): Int {
        return mDeals.size
    }

    private fun removeItem(itemNeedsToBeRemoved: Deal) {
        mDeals.remove(itemNeedsToBeRemoved)
        if (!onBind) {
            notifyDataSetChanged()
        }
    }
}